package storage;

import java.util.*;
import mephi.isa.social_network.Characteristic;

public class Storage {
    private Map<Integer, Characteristic> characteristic_storage;
    private Map<Integer, Post> post_storage;
    private Map<Integer, Comment> comment_storage;
    private SortedMap<Integer, Category> category_storage;

    public Characteristic GetCharacteristic(Integer id) {
        return this.characteristic_storage[id];
    }

    public ArrayList<Characteristic> GetCharacteristics(String name, String description) {
        ArrayList<Characteristic> result = new ArrayList<>();
        for (Map.Entry<Integer, Characteristic> entry : this.characteristic_storage) {
            Characteristic characteristic = entry.getValue());
            if (characteristic.name == name) {
                result.add(characteristic);
            } 
        }

        return result;
    }

    public void InsertCharacteristic(Integer id, Characteristic characteristic) {
        this.characteristic_storage[id] = characteristic;
    }

    public Post GetPost(Integer id) {
        return this.post_storage[id];
    }

    public void InsertPost(Integer id, Post post) {
        this.post_storage[id] = post;
    }

    public void BlockPost(Integer id) {
        this.post_storage[id].setBlocked();
    }

    public void AddCharacteristicToPost(Integer id, Characteristic characteristic) {
        this.post_storage[id].addCharacteristic(characteristic)
    }

    public Comment GetComment(Integer id) {
        return this.comment_storage[id];
    }

    public void InsertComment(Integer id, Comment Comment) {
        this.comment_storage[id] = comment;
    }

    public Characteristic GetCategory(Integer id) {
        return this.category_storage[id];
    }

    public void CreateCategory(Category category) {
        Integer key = this.category_storage.lastKey()
        this.category_storage[key+1] = category;
    }
}