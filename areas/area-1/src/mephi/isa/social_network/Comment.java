package mephi.isa.social_network;

import java.util.Date;
import java.util.UUID;

public class Comment {
    private UUID user;
    private String text;
    private Date dateTime;

    public Comment(UUID user, String text, Date dateTime) {
        this.user = user;
        this.text = text;
        this.dateTime = dateTime;
    }

    public UUID getUser() {
        return user;
    }

    public void setUser(UUID user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
