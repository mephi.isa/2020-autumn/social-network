package mephi.isa.social_network_test;

import mephi.isa.social_network.Category;
import mephi.isa.social_network.Characteristic;
import mephi.isa.social_network.CharacteristicType;
import mephi.isa.social_network.Post;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class CategoryTest {

    private Category category;

    @Test
    void constructorDoubleParam() {
        String name = "test";
        String description = "test description";
        ArrayList<Post> posts = new ArrayList<>();
        ArrayList<Characteristic> characteristics = new ArrayList<>();

        category = new Category(name, description);
        assertEquals(name, category.getName());
        assertEquals(description, category.getDescription());
        assertEquals(posts, category.getPosts());
        assertEquals(characteristics, category.getCharacteristics());
    }

    @Test
    void constructorTripleParam() {
        String name = "test";
        String description = "test description";
        ArrayList<Post> posts = new ArrayList<>();
        ArrayList<Characteristic> characteristics = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            characteristics.add(new Characteristic(String.format("char%d", i), CharacteristicType.INTEGER));
        }
        category = new Category(name, description, characteristics);

        assertEquals(name, category.getName());
        assertEquals(description, category.getDescription());
        assertEquals(posts, category.getPosts());
        assertEquals(characteristics, category.getCharacteristics());
    }

    @Test
    void equality() {
        String name = "test";
        String description = "test description";
        ArrayList<Post> posts = new ArrayList<>();
        ArrayList<Characteristic> characteristics = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            characteristics.add(new Characteristic(String.format("char%d", i), CharacteristicType.INTEGER));
        }
        category = new Category(name, description, characteristics);

        String name1 = "test";
        String description1 = "test description";
        ArrayList<Post> posts1 = new ArrayList<>();
        ArrayList<Characteristic> characteristics1 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            characteristics1.add(new Characteristic(String.format("char%d", i), CharacteristicType.INTEGER));
        }
        Category category1 = new Category(name1, description1, characteristics1);

        assertEquals(category, category);
        assertEquals(category, category1);

        category1.setName("new test");
        assertNotEquals(category, category1);
        category1.setName(name);
        assertEquals(category, category1);
        category1.setDescription("new description");
        assertNotEquals(category, category1);
        assertNotEquals(category, null);
    }

    @Test
    void setName() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);
        String newName = "new";

        category.setName(newName);
        assertEquals(newName, category.getName());
    }

    @Test
    void setDescription() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);
        String newDescription = "new description";

        category.setDescription(newDescription);
        assertEquals(newDescription, category.getDescription());
    }

    @Test
    void setPosts_noThrow() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            posts.add(new Post(new UUID(i + 5,i), String.format("txt%d", i), new Date(), category));
        }
        category.setPosts(posts);
        assertEquals(category.getPosts(), posts);
    }

    @Test
    void setPosts_throwsException() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            posts.add(new Post(new UUID(i + 5,i), String.format("txt%d", i), new Date(), category));
        }
        posts.add(new Post(new UUID(10, 5),
                           "txtother",
                           new Date(),
                           new Category("other", "other")));
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
           category.setPosts(posts);
        });
        String expectedMessage = "Post category not equals this category";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void addPost_noThrow() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            posts.add(new Post(new UUID(i + 5,i), String.format("txt%d", i), new Date(), category));
        }
        category.setPosts(posts);

        Post newPost = new Post(new UUID(12, 5), "new text", new Date(), category);
        category.addPost(newPost);
        assertEquals(newPost, category.getPosts().get(category.getPosts().size() - 1));
    }

    @Test
    void addPost_throwsException() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            posts.add(new Post(new UUID(i + 5,i), String.format("txt%d", i), new Date(), category));
        }
        category.setPosts(posts);

        Post newPost = new Post(new UUID(12, 5), "new text", new Date(), new Category("other", "other"));
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            category.addPost(newPost);
        });
        String expectedMessage = "Post category not equals this category";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void setCharacteristics() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);

        ArrayList<Characteristic> characteristics = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            characteristics.add(new Characteristic(String.format("char%d", i), CharacteristicType.INTEGER));
        }
        category.setCharacteristics(characteristics);
        assertEquals(characteristics, category.getCharacteristics());

        ArrayList<Characteristic> newCharacteristics = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            newCharacteristics.add(new Characteristic(String.format("newChar%d", i), CharacteristicType.STRING));
        }
        category.setCharacteristics(newCharacteristics);
        assertEquals(newCharacteristics, category.getCharacteristics());
    }

    @Test
    void addCharacteristic() {
        String name = "test";
        String description = "test description";
        category = new Category(name, description);

        ArrayList<Characteristic> characteristics = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            characteristics.add(new Characteristic(String.format("char%d", i), CharacteristicType.INTEGER));
        }

        for (Characteristic i : characteristics) {
            category.addCharacteristic(i);
        }
        assertEquals(characteristics, category.getCharacteristics());
    }
}
